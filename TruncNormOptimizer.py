#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Find the parameters of a truncated normal distribution based on known truncation, mean and standard deviation, using non-linear solvers available in scipy.optimize
"""

import sys
import numpy as np
from scipy.stats import truncnorm, norm
import scipy.optimize as opt
from argparse import ArgumentParser

def E(mu, sigma, a, b):
    alpha = (a-mu)/sigma
    beta = (b-mu)/sigma
    phiAlpha = norm.pdf(alpha)
    phiBeta = norm.pdf(beta)
    PhiAlpha = norm.cdf(alpha)
    PhiBeta = norm.cdf(beta)

    return mu + sigma*((phiAlpha-phiBeta)/(PhiBeta-PhiAlpha))

def std(mu, sigma, a, b):
    alpha = (a-mu)/sigma
    beta = (b-mu)/sigma
    phiAlpha = norm.pdf(alpha)
    phiBeta = norm.pdf(beta)
    PhiAlpha = norm.cdf(alpha)
    PhiBeta = norm.cdf(beta)

    return np.sqrt((sigma**2)*(1 + (((alpha*phiAlpha)-(beta*phiBeta))/(PhiBeta-PhiAlpha)) - ((phiAlpha-phiBeta)/(PhiBeta-PhiAlpha))**2))

def optimizeFunc(method, Efinal, StdFinal, aFinal, bFinal):
    """
    """
    def wrap_root(x):
        mu = x[0]
        sigma = x[1]
        return [E(mu, sigma, aFinal, bFinal)-Efinal, std(mu, sigma, aFinal, bFinal)-StdFinal]

    optFunc = getattr(opt, method)
    return optFunc(wrap_root, [Efinal, StdFinal])

def check(x, Efinal, StdFinal, aFinal, bFinal):
    a, b = aFinal, bFinal
    Ecomp = E(x[0], x[1], a, b)
    StdComp = std(x[0], x[1], a, b)
    return (np.isclose(Ecomp, Efinal) and np.isclose(StdComp, StdFinal))

if __name__ == "__main__":
    #parsers and co.
    parser = ArgumentParser(description="Find the parameters of a truncated normal distribution based on known truncation, mean and standard deviation.")
    parser.add_argument("-m", "--mean", dest="mean", type=float, help="mean of end distribution")
    parser.add_argument("-s", "--stddev", dest="stdDev", type=float, help="standard deviation of end distribution")
    parser.add_argument("-a", "--a", dest="a", type=float, help="Left truncation point")
    parser.add_argument("-b", "--b", dest="b", type=float, help="Right truncation point")
    parser.add_argument("-me", "--method", dest="method", default="fsolve", type=str, help="Non-linear solver method: [fsolve, broyden1, broyden2, newton_krylov, anderson, excitingmixing, linearmixing, diagbroyden]")
    parser.add_argument("-sc", "--shortcircuit", dest="shortcircuit", default=False, type=bool, help="if True short circuits the code and prints only expected value and standard deviation of given parameters")
    args = parser.parse_args()

    if args.shortcircuit:
        print("mean = {}\nstddev = {}".format(np.round(E(args.mean, args.stdDev, args.a, args.b),4), np.round(std(args.mean, args.stdDev, args.a, args.b),4)))
        sys.exit()

    x = optimizeFunc(args.method, args.mean, args.stdDev, args.a, args.b)
    if check(x, args.mean, args.stdDev, args.a, args.b):
        print("loc = {}\nscale = {}".format(x[0], x[1]))
    else:
        print("Something went bananas. Check isclose settings maybe? Or boundaries?")
        sys.exit()

    print("mean = {} = {}\nstd deviation = {} = {}]".format(args.mean, np.round(E(x[0], x[1], args.a, args.b),4), args.stdDev, np.round(std(x[0], x[1], args.a, args.b), 4)))
