  Small programm to find the parameters of a truncated normal distribution based on known truncation, mean and standard deviation, using non-linear solvers available in scipy.optimize

# Usage

```
$ python TruncNormOptimizer.py -h
```

shows help and exits.
